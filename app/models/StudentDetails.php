<?php

namespace App\models;


use Illuminate\Database\Eloquent\Model;

class StudentDetails extends Model{
	
	 public $timestamps = false;

	 protected $table = 'student_details';

	 protected $fillable = ['firstname', 'lastname', 'sex','address','department_id', 'is_active'];


	 public function user(){

        return $this->hasOne('App\models\User');
    }

    public function electives(){

    	return $this->belongsToMany('App\models\Elective');
    }

    public function department(){

    	return $this->belongsTo('App\models\Department', 'id');
    }

}



?>