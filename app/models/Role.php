<?php
namespace App\models;

use Illuminate\Database\Eloquent\Model;

class Role extends Model{

	public $timestamps = false;

	 /**
     * The database table used by the model.
     *
     * @var string
     */

	protected $table = 'role';

	protected $hidden = ['pivot', 'id'];
}



?>