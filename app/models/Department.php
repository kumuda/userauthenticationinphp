<?php

namespace App\models;


use Illuminate\Database\Eloquent\Model;
use App\models\User;

class Department extends Model{


	public $timestamps = false;

	 protected $table = 'departments';

	 protected $fillable = ['name'];

	 

}


?>