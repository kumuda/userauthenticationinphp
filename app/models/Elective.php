<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;


class Elective extends Model {

	public $timestamps = false;

	/**
     * The database table used by the model.
     *
     * @var string
     */

	protected $table = 'electives';

	protected $fillable = ['name'];

}



?>