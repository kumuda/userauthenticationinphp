<?php

namespace App\Http\controllers;

use Illuminate\Routing\Controller as BaseController;
use App\models\User;
use App\models\Role;
use App\models\StudentDetails;
use App\models\Department;
use Auth;
use Input;
use Hash;
use Redirect;
use Session;
use DB;

class UserController extends BaseController{


	public function create(){

		if(Auth::check()){

			$user = Auth::user();
			
			foreach($user->roles as $value){

				if($value->name == 'admin'){
			        return Redirect::to('eLearning/home'); 
			    }

			    else if($value->name == 'student'){
			    	return Redirect::to('eLearning/studentHome');
			    }

		    }
		}

		return view('user.login');
	}

	public function destroy(){

		 Auth::logout();
         return Redirect::to('eLearning'); 

	}

	public function store(){


		if(Auth::attempt(Input::only('email','password')))
		{
			//echo "Inside YAY!!";
			$user = Auth::user();
			if($user->is_enabled == true){
				
				foreach ($user->roles as $value) {

						
						if($value->name == 'admin'){

							Session::put('email', $user->email);
							return Redirect::to('eLearning/home'); 
						}
					
						else if ($value->name == 'student'){


							Session::put('email',$user->email);
							$id = DB::table('user_student_detail')->where('user_id',$user->id)->pluck('student_id');
							Session::put('student_id',$id);
							return Redirect::to('eLearning/studentHome');
                         
                         }

					}	

			}
				
		} 
	}


	public function home(){

		$user = Auth::user();
			
			foreach($user->roles as $value){

				if($value->name == 'admin'){

		              return view('user.home');
		          }
		      }
	}

	public function studentHome(){

		$user = Auth::user();
			
			foreach($user->roles as $value){

				if($value->name == 'student'){

		          return view('user.studentHome');
		      }
		  }
	}

}



?>