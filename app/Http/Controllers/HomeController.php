<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller as BaseController;
use App\models\StudentDetails;
use App\models\Elective;
use App\models\Department;
use Illuminate\Http\Request;
use DB;


class HomeController extends BaseController {

	public function show(){

		$studentDetails = StudentDetails::where('is_active',true)->with('department')->get();

		return $studentDetails;

	}


	public function studentDetails(Request $request){

		$id = $request->session()->get('student_id');
		$studentDetails = StudentDetails::where('is_active', true)->where('id','=',$id)->with('electives','department')->get();
		return $studentDetails;
	}

	public function getElectives(Request $request) {

		$id = $request->input('id');

		$student = StudentDetails::find($id);

		return $student->electives;
	}

	public function allElectives(Request $request) {
		
		$departmentid = $request->input('departmentid');

		$electives = Elective::where('department_id', $departmentid)->get();
		
		return $electives;
	}

	public function allDepartments(){
		
		$departments = Department::get();

		return $departments;

	}

	public function editDetails(Request $request){

		$id = $request->input('id');
		if($id == 0){
			return 'error';
		}		
		$student = StudentDetails::find($id);
	    
	    $firstname = $request->input('firstname');
	    if($firstname != NULL){
		$student->firstname = $firstname;
	    }

		$lastname = $request->input('lastname');
	    if($firstname != NULL){
		$student->lastname = $lastname;
	    }

		$sex = $request->input('sex');
		if($sex != NULL){
			$student->sex = $sex;
		}

		$address = $request->input('address');
		if($address != NULL){
			$student->address = $address;
		}


		$name = $request->input('department');
		if($name != NULL){
			$departmentObject = Department::where('name',$name)->get();
			$student->department_id = $departmentObject[0]->id;		
		}

		$electives = $request->input('electives');
		if($electives != NULL){
		DB::table('elective_student_details')->where('student_details_id',$student->id)->delete();

		foreach ($electives as $electiveName) {
			$elective = Elective::where('name',	$electiveName)->get();
			DB::table('elective_student_details')->insert(
                                        array('elective_id' => $elective[0]->id, 'student_details_id' => $student->id)
            );
			
		}

		}

		$student->save();

		return 'success';

	}

	public function deleteStudents(Request $request){

		$ids = $request->input('ids');

		foreach ($ids as $id){
			DB::table('student_details')->where('id',$id)->update(array('is_active'=>false));
		}

		return 'success';

	}

}

