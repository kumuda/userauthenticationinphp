<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

/*Route::get('/','Auth\HomeController@home');
	*/
	
use App\models\User;
use App\models\Role;
use App\models\StudentDetails;
use App\models\Department;
use App\Http\controllers\UserController;
use App\Http\controllers\HomeController;

Route::get('eLearning','UserController@create');
Route::get('eLearning/logout','UserController@destroy');
Route::post('eLearning/login','UserController@store');
Route::get('eLearning/home','UserController@home');
Route::get('eLearning/studentHome','UserController@studentHome');
Route::get('eLearning/studentDetails','HomeController@show');
Route::get('eLearning/getElectives', 'HomeController@getElectives');
Route::get('eLearning/allElectives', 'HomeController@allElectives');
Route::get('eLearning/allDepartments', 'HomeController@allDepartments');
Route::get('eLearning/editDetails', 'HomeController@editDetails');
Route::get('eLearning/deleteStudents','HomeController@deleteStudents');
Route::get('eLearning/getStudent','HomeController@studentDetails');
/*Route::get('/','UserController@index');*/