var departmentid;

$(document)
.ready(
	function() {


				$(window).load(function() {
						view();
					});

$('#department').on('change',function(){
	departmentid = $('#department').find(":selected").val();
	console.log(departmentid);
		var data = {
					'departmentid' : departmentid
				};
				$.ajax({
				type : "GET",
				url : "allElectives",
				data : data,
				success : function(result) {
					console.log(result);
					$('#electives').empty();
					result.forEach(function(elective) {
						$('<input />').attr({type:'checkbox',name:elective.name}).val(elective.name).appendTo('#electives');
						$('<label>' + elective.name + '</label>').appendTo('#electives');
					});
					}
				});		

	});
				
$('#edit').click(function() {
				$('#edit').css({'display':'none'});
				$('#save').css({'display':'-webkit-inline-box'});
				$('form').find('input,textarea,select').removeAttr('disabled');
				$('#elective-labels').css({'display':'none'});
				var electives = $('#elective-labels').text().split(",");
				console.log(electives);
			   // departmentid = $('#departmentid').val();
				$.ajax({
				type : "GET",
				url : "allDepartments",
				success : function(result) {
					console.log(result);
					result.forEach(function(department) {
						$('<option>' + department.name + '</option>').attr({value:department.id}).appendTo($('#department'));
					});
					}
				});	
				console.log(departmentid);
				var data = {
					'departmentid' : departmentid
				};
				$.ajax({
				type : "GET",
				url : "allElectives",
				data : data,
				success : function(result) {
					console.log(result);
					result.forEach(function(elective) {
						$('<input />').attr({type:'checkbox', class:'elective', name:elective.name}).val(elective.name).appendTo('#electives');
						$('<label>' + elective.name + '</label>' + '&nbsp').appendTo('#electives');
					});
					}
				});	
				$('#edit').replaceWith($('#save'));
				$('#save').show();
			});

$('#save').click(function(){

	var id = $('#id').text();
	console.log(id);
	var firstname = $('#firstname').val();
	var lastname = $('#lastname').val();	
	var sex;
	if($('input:radio[name=sex]')[0].checked){
		sex = 'male';
	}
	else{
		sex = 'female';
	}	
	var department = $('#department').find(":selected").text();
	console.log(department);
	var electives= [];
	$('.elective:checked').each(function(){
			electives.push($(this).val());
		  });
    var address = $('#address').val();

    var data = {
    	'id':id,
    	'sex':sex,
    	'firstname':firstname,
    	'lastname':lastname,
    	'department':department,
    	'electives':electives,
    	 'address':address
    };

    $.ajax({
				type : "GET",
				url : "editDetails",
				data : data,
				success : function(result) {
						if(result == "success"){
							alert('Details successfully updated');
						}
					}
				});	

    
view();
});


	});


function view(){

	$('#electives').empty();	
	$.ajax({
					type : "GET",
					url : "getStudent",
					success : function(result) {
						console.log(result);
					var electives = "";
						console.log(result);
						$('#id').text(result[0].id);
						if(result[0].sex == 'male'){
							$('#male-radio').attr({'checked':true});
						}
						else{
							$('#female-radio').attr({'checked':true});
						}
						$('#firstname').val(result[0].firstname);
						departmentid = (result[0].department.id);
						$('#lastname').val(result[0].lastname);
						$('<option>' + result[0].department.name + '</option>').attr({value:$('#departmentid'),"selected":true}).appendTo($('#department'));
						$('#address').val(result[0].address);
						result[0].electives.forEach(function(elective) {
							electives = electives.concat(elective.name);
							electives = electives.concat(", ");
						});

						$('<label>' + electives + '</label>').attr({id:"elective-labels"}).appendTo("#electives");
					}

				});


}