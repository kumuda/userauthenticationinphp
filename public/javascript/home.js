var paginationData = {"pagenumber":1};
var folderName = 'inbox';
var folderLink = null;

function display(data) {

	var table = $("#table");
	var row;
	$("#table tbody tr:not(.table-header)").remove();
	var emailSelect;
	var departmentid;

	data.forEach(function(email) {
		row = $("<tr></tr>");
		emailSelect = $("<td />").addClass("email-select").append($("<input />").attr({
			type : "checkbox",
			class : "checkbox",
			value : email.id
		}))
		emailSelect.change(displayDeleteButton());
		emailSelect.appendTo(row);
		$("<td>" + email.id + "</td>").appendTo(row);
		$("<td>" + email.firstname + "</td>").appendTo(row);
		$("<td>" + email.lastname + "</td>").appendTo(row);
		$("<td>" + email.sex + "</td>").appendTo(row);
		$("<td>" + email.address + "</td>").appendTo(row);
		$("<td>" + email.department.name + "</td>").appendTo(row);
		$("<td>" + email.department.id + "</td>").attr({id:"departmentid"}).css({
			"display" : "none"
		}).appendTo(row);
		
		row.click(view());
		row.appendTo(table);
	});
}

$(document)
.ready(
	function() {

		folderLink = $('#inbox_link');
		$("#message_body").jqte();

		$(window).load(function() {
						//$('#loadModal').modal('show');
						getStudents(1, true, folderName,'');

					});

$('#refresh').on('click',function(){
					//	$('#loadModal').modal('show');
					getStudents(1, true, folderName,'');
				});

$('#department').on('change',function(){
	departmentid = $('#department').find(":selected").val();
	console.log(departmentid);
		var data = {
					'departmentid' : departmentid
				};
				$.ajax({
				type : "GET",
				url : "allElectives",
				data : data,
				success : function(result) {
					console.log(result);
					$('#electives').empty();
					result.forEach(function(elective) {
						$('<input />').attr({type:'checkbox',name:elective.name}).val(elective.name).appendTo('#electives');
						$('<label>' + elective.name + '</label>').appendTo('#electives');
					});
					}
				});		

	});

$('#edit').click(function() {
				$('form').find('input,textarea,select').removeAttr('disabled');
				$('#elective-labels').css({'display':'none'});
				var electives = $('#elective-labels').text().split(",");
				console.log(electives);
				departmentid = $('#departmentid').text();
				$.ajax({
				type : "GET",
				url : "allDepartments",
				success : function(result) {
					console.log(result);
					result.forEach(function(department) {
						$('<option>' + department.name + '</option>').attr({value:department.id}).appendTo($('#department'));
					});
					}
				});	
				console.log(departmentid);
				var data = {
					'departmentid' : departmentid
				};
				$('#electives').empty();
				$.ajax({
				type : "GET",
				url : "allElectives",
				data : data,
				success : function(result) {
					console.log(result);
					result.forEach(function(elective) {
						$('<input />').attr({type:'checkbox', class:'elective', name:elective.name}).val(elective.name).appendTo('#electives');
						$('<label>' + elective.name + '</label>').appendTo('#electives');
					});
					}
				});	
				$('#edit').css({'display':'none'});

				$('#save').css({'display':'inline-block'});
			});


$('#save').click(function(){

	var id = $('#id').text();
	console.log(id);
	var firstname = $('#firstname').val();
	var lastname = $('#lastname').val();	
	var sex;
	if($('input:radio[name=sex]')[0].checked){
		sex = 'male';
	}
	else{
		sex = 'female';
	}	
	var department = $('#department').find(":selected").text();
	console.log(department);
	var electives= [];
	$('.elective:checked').each(function(){
			electives.push($(this).val());
		  });
    var address = $('#address').val();

    var data = {
    	'id':id,
    	'sex':sex,
    	'firstname':firstname,
    	'lastname':lastname,
    	'department':department,
    	'electives':electives,
    	 'address':address
    };

    $.ajax({
				type : "GET",
				url : "editDetails",
				data : data,
				success : function(result) {
						if(result == "success"){
							alert('Details successfully updated');
						}
					}
				});	

    
$('#emailModal').modal('hide');
getStudents(1, true, folderName,'');


});

$('#delete').on('click',function(){
		var ids = [];
		
		$('.checkbox:checked').each(function(){
			ids.push($(this).val());
		  });
		if(ids[0] != null){
			var data = {
    	'ids':ids,
    };

    $.ajax({
				type : "GET",
				url : "deleteStudents",
				data : data,
				success : function(result) {
						if(result == "success"){
							alert('Deleted successfully');
							getStudents(1, true, folderName,'');

						}
					}
				});	
		}
		else{
			alert('Please Select an Item to Delete.');
		}
	});

});

function displayDeleteButton() {
	
	$('.email-select').unbind().on('change',function(){
		if($('.checkbox:checked').length>0){
			$('#deleteMails').css({'display':'block'});
		} 
		else{
			$('#deleteMails').css({'display':'none'});
		}
	});
}


function pagination(pageNos, folderName,searchString) {

	$('.pagination').bootpag({
		total : pageNos,
		page : 1,
		maxVisible : 10
	}).on('page', function(event, num) {
		paginationData.pagenumber = num;
		//$('#loadModal').modal('show');
		getStudents(num, false, folderName,searchString); // or some ajax content
												// loading...
											});

}


function getStudents(pageNo, setPagination, folderName,searchString) {
	var students = [];
	//$.get("/Webclient/" + folderName + "?pageNo=" + pageNo +"&search=" + searchString, function(data) {
		$.get("studentDetails", function(data) {
			if (data != null) {
				console.log(data);
			//data = JSON.parse(data);
			students = data;
		} 
	   //   $('#loadModal').modal('toggle');

	   if(students.length > 0){
	   	display(students);
		//var totalPageNos = data.totalPageNos;
		var totalPageNos = 10;
		if (setPagination) {
			pagination(totalPageNos, folderName,searchString);
		}
		folderLink.addClass('bold');
	}
	else{
		alert("No students present.");
	}
});
	}

	var editViewForm = function(edit){
		$('form').find('input,textarea,select').each(function(index, element){
					$(element).attr('disabled', edit);
				});
	}

	function view() {


		$('#table tbody').unbind().on(
			'click',
			'tr td:not(.email-select)',
			function() {

				editViewForm(true);
		        $('#edit').css({'display':'inline-block'});
		        $('#save').css({'display':'none'});


				var id = $(this).parent().children('td:eq(0)')
				.children().val();
				var firstname = $(this).parent().children('td:eq(2)').text();

				var lastname = $(this).parent().children('td:eq(3)').text();
				var sex = $(this).parent().children('td:eq(4)').text();
				var address = $(this).parent().children('td:eq(5)').text();
				var department = $(this).parent().children('td:eq(6)').text();
				var data = {
					'id' : id
				};

				$('#electives').empty();
				$.ajax({
					type : "GET",
					url : "getElectives",
					data : data,
					success : function(result) {

					var electives = "";
						console.log(result);
						$('#id').text(id);
						if(sex == 'male'){
							$('#male-radio').attr({'checked':true});
						}
						else{
							$('#female-radio').attr({'checked':true});
						}
						$('#firstname').val(firstname);
						$('#lastname').val(lastname);
						$('<option>' + department + '</option>').attr({value:$('#departmentid'),"selected":true}).appendTo($('#department'));
						$('#address').val(address);
						result.forEach(function(elective) {
							electives = electives.concat(elective.name);
							electives = electives.concat(", ");
						});

						$('<label>' + electives + '</label>').attr({id:"elective-labels"}).appendTo("#electives");
						$('#emailModal').modal('show');
					}

				});


			});

}

