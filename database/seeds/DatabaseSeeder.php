<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        // $this->call(UserTableSeeder::class);

        DB::table('user')->insert([
            'id' => 1,
            'name' => 'Kumuda',
            'email' => 'kumuda.mulgund',
            'password' => Hash::make('kumuda'),/*bcrypt('kumuda'),*/
            'is_enabled' => true
        ]);

        DB::table('user')->insert([
            'id' => 2,
            'name' => 'madhuri',
            'email' => 'madhuri.mulgund',
            'password' => Hash::make('madhuri'),
            'is_enabled' => true
        ]);

        DB::table('user')->insert([
            'id' => 3,
            'name' => 'uday',
            'email' => 'uday.reddy',
            'password' => Hash::make('uday'),
            'is_enabled' => true
        ]);

        DB::table('role')->insert([
            'id' => 1,
            'name' => 'admin'
        ]);

        DB::table('role')->insert([
            'id' => 2,
            'name' => 'student'
        ]);

        DB::table('role_user')->insert([
            'user_id' => 1,
            'role_id' => 1
        ]);

        DB::table('role_user')->insert([
            'user_id' => 1,
            'role_id' => 2
        ]);

        DB::table('role_user')->insert([
            'user_id' => 2,
            'role_id' => 2
        ]);

        DB::table('role_user')->insert([
            'user_id' => 3,
            'role_id' => 2
        ]);

        DB::table('departments')->insert([
            'id' => 1,
            'name' => 'computer science'
        ]);

        DB::table('departments')->insert([
            'id' => 2,
            'name' => 'electrical and electronics'
        ]);

         DB::table('departments')->insert([
            'id' => 3,
            'name' => 'civil engineering'
        ]);

         DB::table('electives')->insert([
            'id' => 1,
            'name' => 'networks',
            'department_id' => 1
        ]);


         DB::table('electives')->insert([
            'id' => 2,
            'name' => 'software testing',
            'department_id' => 1
        ]);

         DB::table('electives')->insert([
            'id' => 3,
            'name' => 'machine design',
            'department_id' => 2
        ]);

         DB::table('electives')->insert([
            'id' => 4,
            'name' => 'signals and systems',
            'department_id' => 2
        ]);

         DB::table('electives')->insert([
            'id' => 5,
            'name' => 'concrete technology',
            'department_id' => 3
        ]);

         DB::table('electives')->insert([
            'id' => 6,
            'name' => 'geology',
            'department_id' => 3
        ]);

         DB::table('student_details')->insert([
            'id' => 1,
            'firstname' => 'kumuda',
            'lastname' => 'mulgund',
            'sex' => 'female',
            'address' => 'kormangala, Bangalore, India',
            'department_id' => 2,
            'is_active' => true
        ]);

         DB::table('student_details')->insert([
            'id' => 2,
            'firstname' => 'uday',
            'lastname' => 'reddy',
            'sex' => 'male',
            'address' => 'Kamakya, Bangalore, India',
            'department_id' => 1,
            'is_active' => true
        ]);

         DB::table('student_details')->insert([
            'id' => 3,
            'firstname' => 'madhuri',
            'lastname' => 'mulgund',
            'sex' => 'female',
            'address' => 'tilakwadi, Belgaum, India',
            'department_id' => 3,
            'is_active' => true
        ]);

         DB::table('elective_student_details')->insert([
            'elective_id' => 3,
            'student_details_id' => 1
        ]);

          DB::table('elective_student_details')->insert([
            'elective_id' => 4,
            'student_details_id' => 1
        ]);

           DB::table('elective_student_details')->insert([
            'elective_id' => 1,
            'student_details_id' => 2
        ]);

            DB::table('elective_student_details')->insert([
            'elective_id' => 2,
            'student_details_id' => 2
        ]);

          DB::table('elective_student_details')->insert([
            'elective_id' => 5,
            'student_details_id' => 3
        ]);  

         DB::table('elective_student_details')->insert([
            'elective_id' => 6,
            'student_details_id' => 3
        ]); 

         DB::table('user_student_detail')->insert([
            'user_id' => 1,
            'student_id' => 1
        ]);

          DB::table('user_student_detail')->insert([
            'user_id' => 2,
            'student_id' => 3
        ]);

           DB::table('user_student_detail')->insert([
            'user_id' => 3,
            'student_id' => 2
        ]);


        Model::reguard();
    }
}
