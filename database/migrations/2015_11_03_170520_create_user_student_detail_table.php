<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserStudentDetailTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_student_detail', function (Blueprint $table) {
            $table->integer('user_id');
            $table->integer('student_id');
            $table->foreign('user_id')->references('id')->on('user');
            $table->foreign('student_id')->references('id')->on('student_details');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('user_student_detail');
    }
}
