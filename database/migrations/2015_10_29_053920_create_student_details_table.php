<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStudentDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       Schema::create('student_details', function (Blueprint $table) {
            $table->integer('id')->increments();
            $table->string('firstname');
            $table->string('lastname');
            $table->string('sex');
            $table->string('address');
            $table->boolean('is_active');
            $table->primary('id');
            $table->integer('department_id');
            $table->foreign('department_id')->references('id')->on('departments');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('student_details');
    }
}
