<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('user', function (Blueprint $table) {
            $table->integer('id')->increments();
            $table->string('name');
            $table->string('email');
            $table->string('password');
            $table->string('remember_token');
            $table->boolean('is_enabled');
            $table->unique(array('email'));
            $table->primary('id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //

        Schema::drop('user');
    }
}
