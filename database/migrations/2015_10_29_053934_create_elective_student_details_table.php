<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateElectiveStudentDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('elective_student_details', function (Blueprint $table) {
            $table->integer('elective_id');
            $table->integer('student_details_id');
            $table->foreign('elective_id')->references('id')->on('electives');
            $table->foreign('student_details_id')->references('id')->on('student_details');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('elective_student_details');
    }
}
