<html>


<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" type="text/css" href="{{ URL::asset('css/bootstrap.css') }}">
<link rel="stylesheet" type="text/css" href="{{ URL::asset('css/jquery-te-1.4.0.css') }}">
<link rel="stylesheet" type="text/css" href="{{ URL::asset('css/custom.css') }}">
<link rel="stylesheet" type="text/css" href="{{ URL::asset('css/bootstrap.css.map') }}">
<link rel="stylesheet" type="text/css" href="{{ URL::asset('css/bootstrap-table.css') }}">
<!--<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">-->
<script type="text/javascript" src="{{ URL::asset('javascript/jquery.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('javascript/bootstrap.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('javascript/bootstrap-table.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('javascript/bootstrap-table-locale-all.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('javascript/jquery-te-1.4.0.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('javascript/studentHome.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('javascript/jquery.form.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('javascript/jquery.bootpag.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('javascript/jquery.autocomplete.js') }}"></script>
<title>E-learning</title>
</head>


<body>

<div class="login__header">
		<div class="preview__envato-logo">
			<h4>Student E-Learning</h4>
		</div>
		<div class="container">
			<ul class="nav navbar-nav navbar-right">
				<div class="row">
					<div class="col-mod-6">
						<span class="pull-right"><a href="logout"
							style="color: black">Logout </a> </span>
					</div>
				</div>
			</ul>
		</div>
	</div>

	<div class="spacer50"></div>

		<div class="container">
								<form role="form" name="view" class="form-horizontal" align="left">
				<div class="form-group row">
					<div class="col-md-2 col-md-offset-3">
						<label for="id" id="student_id">Id</label>
					</div>
					<div class="col-md-5">
						<label type="text" class="form-control focusedInput" id="id" name="id" disabled ></label>
							<div class="alert alert-danger" role="alert" style="margin-bottom: 10px;" id="danger" hidden>
  							Song title cannot be empty.
						</div>

					</div>
				</div>

				<div class="form-group row">
					<div class="col-md-2 col-md-offset-3">
						<label for="firstname">Sex</label>
					</div>
					<div class="col-md-5">
						<input type="radio"  id="male-radio"
							 name="sex" disabled />
						<label id="male" >Male</label>	 

						<input type="radio"  id="female-radio"
							 name="sex" disabled />
						<label id="female" >Female</label>	 

					</div>
				</div>


				<div class="form-group row">
					<div class="col-md-2 col-md-offset-3">
						<label for="firstname">FirstName</label>
					</div>
					<div class="col-md-5">
						<input type="text" class="form-control focusedInput" id="firstname"
							 name="firstname" disabled />

					</div>
				</div>


				<div class="form-group row">
					<div class="col-md-2 col-md-offset-3">
						<label for="lastname">LastName</label>
					</div>
					<div class="col-md-5">
						<input type="text" class="form-control focusedInput" id="lastname"
							 name="lastname" disabled />
					</div>
				</div>


				<div class="form-group row">
					<div class="col-md-2 col-md-offset-3">
						<label for="department">Department</label>
					</div>
					<div class="col-md-5">
						<select class="form-control focusedInput" id="department" name="department" disabled>
						</select>
					</div>
				</div>


				<div class="form-group row">
					<div class="col-md-2 col-md-offset-3">
						<label for="electives">Electives</label>
					</div>
					<div class="col-md-7" id="electives">
						
					</div>
				</div>

				<div class="form-group row">
					<div class="col-md-2 col-md-offset-3">
						<label for="address">Address</label>
					</div>
					<div class="col-md-5">
						<textarea type="textarea" class="form-control focusedInput" id="address"
							 name="address" disabled> </textarea>

					</div>
				</div>
			</form>

			         <div class="container" style="text-align:center">
						<button type="button" class="btn btn-primary" id="edit" >
						<span class="glyphicon glyphicon-edit">Edit</span>
						</button>

						<button type="button" class="btn btn-primary" id="save" style="display:none">
						<span class="glyphicon glyphicon-save"> </span>
						</button>
			        </div>

			        <input type='text' id='departmentid' style="display:none"/>
		</div>

</body>

</html>