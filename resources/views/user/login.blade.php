<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Login</title>
<link rel="stylesheet" type="text/css" href="css/bootstrap.css.map">
<link rel="stylesheet" type="text/css" href="css/bootstrap.css">
<link rel="stylesheet" type="text/css" href="css/custom.css">
<script src="javascript/jquery.js"></script>
<script src="javascript/bootstrap.js"></script>
</head>
<body>
	<div class="login__header">
		<div class="preview__envato-logo">
			<h4>Student E-Learning</h4>
		</div>
	</div>

	<div class="login_main_cont">
		<div class="login_cont group login">
				<div class="login_form modal-signup">
					<h2>Login</h2>
					<form method="post" name="login" action="eLearning/login"
						class="formClass">
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
						<fieldset>
							<input type="text" class="form-control focusedInput margin-top-10"
								id="email" placeholder="email" name='email' required />
						</fieldset>
						
						<fieldset>
							<input type="password" class="form-control focusedInput margin-top-10"
								id="password" placeholder="password" name='password'
								required />
						</fieldset>
						
						<div class="alert alert-danger margin-top-10" style="margin-bottom: 10px;" hidden>
							User Name or Password incorrect.
						</div>

						<fieldset class="login_submit">
							<button type="submit" class="buton margin-top-10">SIGN IN</button>
						</fieldset>
					</form>
					<div class="spacer10"></div>
					<a class="link-sign" href="register" align="center" >Don't have
						an account?&nbsp;Sign up!</a>
				</div>
		</div>
	</div>
	</div>
</body>
</html>
