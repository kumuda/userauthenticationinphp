<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<link rel="stylesheet" type="text/css" href="{{ URL::asset('css/bootstrap.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ URL::asset('css/jquery-te-1.4.0.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ URL::asset('css/custom.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ URL::asset('css/bootstrap.css.map') }}">
	<link rel="stylesheet" type="text/css" href="{{ URL::asset('css/bootstrap-table.css') }}">
	<!--<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">-->
	<script type="text/javascript" src="{{ URL::asset('javascript/jquery.js') }}"></script>
	<script type="text/javascript" src="{{ URL::asset('javascript/bootstrap.js') }}"></script>
	<script type="text/javascript" src="{{ URL::asset('javascript/bootstrap-table.js') }}"></script>
	<script type="text/javascript" src="{{ URL::asset('javascript/bootstrap-table-locale-all.js') }}"></script>
	<script type="text/javascript" src="{{ URL::asset('javascript/jquery-te-1.4.0.js') }}"></script>
	<script type="text/javascript" src="{{ URL::asset('javascript/home.js') }}"></script>
	<script type="text/javascript" src="{{ URL::asset('javascript/jquery.form.js') }}"></script>
	<script type="text/javascript" src="{{ URL::asset('javascript/jquery.bootpag.js') }}"></script>
	<script type="text/javascript" src="{{ URL::asset('javascript/jquery.autocomplete.js') }}"></script>
	<title>E-learning</title>
</head>
<body>
	<div class="login__header">
		<div class="preview__envato-logo">
			<h4>Student E-Learning</h4>
		</div>
		<div class="container">
			<ul class="nav navbar-nav navbar-right">
				<div class="row">
					<div class="col-mod-6">
						<span class="pull-right"><a href="logout"
							style="color: black">Logout </a> </span>
						</div>
					</div>
				</ul>
			</div>
		</div>

		<div class="spacer100"></div>
		<div class="container">
			<div align="left" width="100%">
				<button type="button" name="Delete" id="delete"
				class=" btn btn-default" style="color:indianred">
				<span class="glyphicon glyphicon-trash"></span>
			</button>
		</div>
	</div>

	<div class="spacer10"></div>
	<div class="container">
		<input type="hidden" name="_token" value="{{ csrf_token() }}">
		<div class="panel panel-default">
			<!-- Default panel contents -->
			<div class="panel-heading" >Students</div>
			<table class="table" id="table">
				<tbody> 
					<tr class="table-header">
						<td>
						</td>
						<td>
							Id
						</td>
						<td>
							First Name
						</td>
						<td>
							Last Name
						</td>
						<td>
							Sex
						</td>
						<td>
							Address
						</td>
						<td>
							Department
						</td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>

	<div class="modal fade" id="composeModal" tabindex="-1" role="dialog"
	aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"
				aria-label="Close">
				<span aria-hidden="true">&times;</span>
			</button>
			<h4 class="modal-title" id="myModalLabel">New Message</h4>
		</div>
		<div class="modal-body message-body">
			<form action="sendMail" method="post" id="send_mail_form">
				<label>To</label><input type="text"
				class="form-control focusedInput" id="to_email_ids" name="to" /> <label>Subject</label><input
				type="text" class="form-control focusedInput" id="subject"
				name="subject" />
				<div class="spacer30"></div>
				<textarea class="form-control" id="message_body" style="height: 200px" name="messageBody"></textarea>
			</form>
		</div>
		<div id="attachment_names"></div>
		<div class="modal-footer">
			<div class="pull-left">
				
				<input type="file" id="attachment" multiple name="attachment">

			</div>
			<button type="button" class="btn btn-primary" id="send_mail">Send</button>
			<button type="button" class="btn btn-default" name="discard draft"
			id="discard_draft" data-dismiss="modal">
			<span class="glyphicon glyphicon-trash"> </span>
		</button>
	</div>
</div>
</div>
</div>



<div class="modal fade" id="emailModal" tabindex="-1" role="dialog"
aria-labelledby="myModalLabel" aria-hidden="true">
<div class="modal-dialog">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal"
			aria-label="Close">
			<span aria-hidden="true">&times;</span>
		</button>
		<h4 class="modal-title" >Student Details</h4>
	</div>
	<div class="modal-body" class="container-fluid" id="email_body">
		<div>
			<form role="form" name="view" class="form-horizontal" align="left">
				<div class="form-group row">
					<div class="col-md-1 col-md-offset-4">
						<label for="id" id="student_id">Id</label>
					</div>
					<div class="col-md-5">
						<label type="text" class="form-control focusedInput" id="id" name="id" disabled ></label>
						<div class="alert alert-danger" role="alert" style="margin-bottom: 10px;" id="danger" hidden>
							Song title cannot be empty.
						</div>

					</div>
				</div>

				<div class="form-group row">
					<div class="col-md-2 col-md-offset-3">
						<label for="firstname">Sex</label>
					</div>
					<div class="col-md-5">
						<input type="radio" id="male-radio"
						name="sex" disabled />
						<label id="male" >Male</label>	 

						<input type="radio"  id="female-radio"
						name="sex" disabled />
						<label id="female" >Female</label>	 

					</div>
				</div>


				<div class="form-group row">
					<div class="col-md-2 col-md-offset-3">
						<label for="firstname">FirstName</label>
					</div>
					<div class="col-md-5">
						<input type="text" class="form-control focusedInput" id="firstname"
						name="firstname" disabled />

					</div>
				</div>


				<div class="form-group row">
					<div class="col-md-2 col-md-offset-3">
						<label for="lastname">LastName</label>
					</div>
					<div class="col-md-5">
						<input type="text" class="form-control focusedInput" id="lastname"
						name="lastname" disabled />
					</div>
				</div>


				<div class="form-group row">
					<div class="col-md-2 col-md-offset-3">
						<label for="department">Department</label>
					</div>
					<div class="col-md-5">
						<select class="form-control focusedInput" id="department" name="department" disabled>
						</select>
					</div>
				</div>


				<div class="form-group row">
					<div class="col-md-2 col-md-offset-3">
						<label for="electives">Electives</label>
					</div>
					<div class="col-md-7" id="electives">
						
					</div>
				</div>

				<div class="form-group row">
					<div class="col-md-2 col-md-offset-3">
						<label for="address">Address</label>
					</div>
					<div class="col-md-5">
						<textarea type="textarea" class="form-control focusedInput" id="address"
						name="address" disabled> </textarea>

					</div>
				</div>
			</form>
		</div>
		<div class="modal-footer">
			<button type="button" class="btn btn-primary" id="edit">
				<span class="glyphicon glyphicon-edit"></span>Edit
			</button>

			<button type="button" class="btn btn-primary" id="save" style="display:none">
				<span class="glyphicon glyphicon-save"></span>Save
			</button>

			<button type="button" class="btn btn-default" name="cancel"
			id="cancel" data-dismiss="modal">Cancel
		</button>
	</div>
</div>
</div>
</div>
</div>


<div class="modal fade" id="loadModal" tabindex="-1" role="dialog" data-backdrop="false"
aria-labelledby="myModalLabel" aria-hidden="true">
<div class="modal-dialog">
	<div class="modal-content center-div container-fluid">
		<img alt="images/updating.gif" src="images/updating.gif" />
	</div>
</div>
</div>


</body>
</html>
