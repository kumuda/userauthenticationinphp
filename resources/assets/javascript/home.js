var paginationData = {"pagenumber":1};
var folderName = 'inbox';
var folderLink = null;
function inboxDisplay(data) {

	var table = $("#table");
	var row;
	var emailSelect;
	
	//$("#table tbody").remove();
	data.forEach(function(email) {
		row = $("<tr></tr>");
		emailSelect = $("<td />").addClass("email-select").append($("<input />").attr({
			type : "checkbox",
			class : "checkbox",
			value : email.messageNumber
		}))
		emailSelect.change(displayDeleteButton());
		emailSelect.appendTo(row);
		$("<td>" + email.name + "</td>").appendTo(row);
		$("<td>" + email.sex + "</td>").appendTo(row);
		$("<td>" + email.address + "</td>").appendTo(row);
		$("<td>" + email.dept + "</td>").appendTo(row);
		$("<td>" + email.electives + "</td>").appendTo(row);
		$("<td>" + email.seen + "</td>").css({
			"display" : "none"
		}).appendTo(row);
		$("<td>" + email.folderName + "</td>").css({
			"display" : "none"
		}).appendTo(row);
		if (email.seen == false) {
			row.addClass("bold");
		}
		row.click(viewMail());
		row.appendTo(table);
	});
}

$(document)
		.ready(
				function() {
					
					folderLink = $('#inbox_link');
					$("#message_body").jqte();

					$(window).load(function() {
						//$('#loadModal').modal('show');
						getMessages(1, true, folderName,'');

					});

					$('#compose').click(function() {
						$('#composeModal').modal('show');
						var to = $('#to_email_ids').val('');
						var subject = $('#subject').val('');
						var snippet = $('#message_body').val('');
					});

					$('#send_mail')
							.click(
									function() {

										var to = $('#to_email_ids').val();
										var subject = $('#subject').val();
										var snippet = $('#message_body').val();
										var status = null;
										var data = {
											'to' : to,
											'subject' : subject,
											'snippet' : snippet
										};

										if (to.length != 0) {
											$
													.ajax({
														type : "POST",
														url : "/Webclient/sendMail",
														contentType : "application/json; charset=utf-8",
														data : JSON
																.stringify(data),
														success : function(
																result) {

															console.log(result);
															alert("Mesage sent successfully.");
														}

													});

										} else {
											alert('To: field left blank.');
										}

										$('#composeModal').modal('toggle');
										
									});

					$('#table').hover(function() {
						$('#table').css({
							'cursor' : 'pointer'
						});
					});

					$('#inbox_link').on('click', function() {
					//	$('#loadModal').modal('show');
						folderName = 'inbox';
						folderLink.removeClass('bold');
						folderLink = $('#inbox_link');
						getMessages(1, true, "inbox",'');
					});

					$('#sent_link').on('click', function() {
						//$('#loadModal').modal('show');
						folderName = 'sent';
						folderLink.removeClass('bold');
						folderLink = $('#sent_link');
						getMessages(1, true, "sent",'');
						
					});

					$('#drafts_link').on('click', function() {
					//	$('#loadModal').modal('show');
						folderName = 'drafts';
						folderLink.removeClass('bold');
						folderLink = $('#drafts_link');
						getMessages(1, true, "drafts",'');
					});

					$('#trash_link').on('click', function() {
					//	$('#loadModal').modal('show');
						folderName = 'trash';
						folderLink.removeClass('bold');
						folderLink = $('#trash_link');
						getMessages(1, true, "trash",'');
					});

					$('#deleteMails')
							.on(
									'click',
									function() {
										var messageNumbers = [];
										$('.checkbox:checked').each(function() {
											messageNumbers.push($(this).val());

										});
										var folderName = $('.checkbox:checked')
												.parent().parent().children(
														'td:eq(5)').text();
										
										var data = {'messageNumbers':messageNumbers.toString(),
												    'folderName':folderName
												    };
										$.ajax({
											type : "POST",
											url : "/Webclient/deleteMails",
											data : data,
											success : function(result) {
												result = JSON.parse(result);
												if(result.message == 500){
													alert("Could not delete the selected mails.")
												}
												
												else if(result.message == 200){
												//	$('#loadModal').modal('show');
													$('#deleteMails').css({'display':'none'});
													getMessages(paginationData.pagenumber,false,folderName,'');
												}
											}

										});
									});
					
					$('#to_email_ids').autocomplete(
							{
								serviceUrl: "/Webclient/getAddressList",
								paramName: "address"
							});
					
					//$('#attachment').fileupload({ 
							//url: '/Webclient/uploadAttachment',
							//sequentialUploads:true,
							//done: function (e, data){
								
									//console.log(data);
								   
							//}
					//});
					
					$('#compose').on('click',function(){
						
						 $.ajax({
						    	type: 'GET',
						        url: '/Webclient/deleteOutbox',
						       
						        
						 });
					});
					
					$('#refresh').on('click',function(){
					//	$('#loadModal').modal('show');
						getMessages(1, true, folderName,'');
					});
					
					$('#mark').on('change',function(){
						var mark = $('#mark').val();
						console.log(mark);
						if(mark == 'none'){
							$('.email-select').children().prop('checked', false);
							$('#deleteMails').css({'display':'none'});
						
						}
						
						if(mark=='all'){
							$('.email-select').children().prop('checked', true);
							$('#deleteMails').css({'display':'block'});
						}
						
						if(mark == 'unRead'){
							$('.email-select').children().prop('checked', false);
							var unread =  $("tbody > .bold input");
							unread.prop('checked',true);
							$('#deleteMails').css({'display':'block'});
						}
						
						if(mark == 'read'){
							$('.email-select').children().prop('checked', false);
							$("tr:not('[class^=bold]') input").prop('checked',true);
							$('#deleteMails').css({'display':'block'});
						}
						
						
					});
					
		  $("#search").keypress(function(){
					 	var searchString = $("#search").val();
					 	getMessages(1, true, folderName,searchString);
					 	
		  });
		  
		  $('#searchButton').on('click',function(){
			  var searchString = $("#search").val();
			  if(searchString != null && searchString != ''){
			 	getMessages(1, true, folderName,searchString);
			  }
		  });
			 	
});

function setUsername() {

	$('#username').text(username);
}

function displayDeleteButton() {
	
	$('.email-select').unbind().on('change',function(){
		if($('.checkbox:checked').length>0){
			$('#deleteMails').css({'display':'block'});
		} 
		else{
			$('#deleteMails').css({'display':'none'});
		}
	});
}


function pagination(pageNos, folderName,searchString) {

	$('.pagination').bootpag({
		total : pageNos,
		page : 1,
		maxVisible : 10
	}).on('page', function(event, num) {
		paginationData.pagenumber = num;
		//$('#loadModal').modal('show');
		getMessages(num, false, folderName,searchString); // or some ajax content
												// loading...
	});

}


function getMessages(pageNo, setPagination, folderName,searchString) {
	
	//$.get("/Webclient/" + folderName + "?pageNo=" + pageNo +"&search=" + searchString, function(data) {
	$.get("/eLearning/studentDetails", function(data) {
		if (data != null) {
			//data = JSON.parse(data);
			console.log(data);
			var students = data.students;
		} 
	   //   $('#loadModal').modal('toggle');
	});
	//Testing.
	/*var students = [
		{
			"name": 'Uday',
				"sex": 'Male',
				"address": 'Kamakya, Bengaluru',
				"dept": 'Computer Science',
				"electives": 'Web-2.0, Robotics'
		},
		{
			"name": 'Kumuda',
			"sex": 'Female',
			"address": 'Koramangala, Bengaluru',
			"dept": 'Electrical engineering',
			"electives": 'Web-2.0, Matlab'
		}*/
	]

	if(students.length > 0){
		inboxDisplay(students);
		//var totalPageNos = data.totalPageNos;
		var totalPageNos = 10;
		if (setPagination) {
			pagination(totalPageNos, folderName,searchString);
		}
		folderLink.addClass('bold');
	}
	else{
		alert("No students present.");
	}
}

function viewMail() {

	$('#table tbody').unbind().on(
			'click',
			'tr td:not(.email-select)',
			function() {

				var messageNumber = $(this).parent().children('td:eq(0)')
						.children().val();
				var from = $(this).parent().children('td:eq(1)').text();
				var subject = $(this).parent().children('td:eq(2)').text();
				var dateAndTime = $(this).parent().children('td:eq(3)').text();
				var seen = $(this).parent().children('td:eq(4)').text();
				var folderName = $(this).parent().children('td:eq(5)').text();
				var data = {
					'messageNumber' : messageNumber,
					'seen' : seen,
					'folderName' : folderName
				};
				$(this).parent().removeClass("bold");
				$.ajax({
					type : "POST",
					url : "/Webclient/showMail",
					data : data,
					success : function(result) {
						result = JSON.parse(result);
						$('#email_subject').text(subject);
						$('#sender_name').text(from);
						$('#date_and_time').text(dateAndTime);
						$('#messageBody').empty();
						$('#messageBody').append(result.messageBody);
						if(result.attachments != null){
							var table = $('#attachments');
						//	table.remove();
							var row ;
							var hyperlink;
							
							result.attachments.forEach(function(attachment){
								row = $("<tr></tr>")
								hyperlink = $("<a>"+attachment.fileName+"</a>").attr({"href":"#","download":attachment.fileName,"onclick":download(attachment.fileLocation)});
								
								$("<td />").addClass('file-select').css('background-color', 'wheat').append(hyperlink).appendTo(row);
								$("<td>"+ attachment.messageNumber + "</td>").css({
									"display" : "none"
								}).appendTo(row);
								$("<td>"+ attachment.folderName + "</td>").css({
									"display" : "none"
								}).appendTo(row);
								/*row.click(download());*/
								row.appendTo(table);
							});
						
						}
						$('#emailModal').modal('show');
						
					}

				});

			});

}


function download(location){
	
	$('#attachments').unbind().on(
			'click',
			'tr td',
			function() {
	
				//var fileName = $(this).parent().children('td:eq(0)').text();
				var messageNumber = $(this).parent().children('td:eq(1)').text();
				var folderName = $(this).parent().children('td:eq(2)').text();
				console.log(location);
				var data = {
						'messageNumber' : messageNumber,
						//'fileName' : fileName,
						'folderName' : folderName,
						'fileLocation' : location
					};
				
				window.location = "/Webclient/downloadAttachment?messageNumber=" + messageNumber + "&folderName=" + folderName + "&fileLocation=" + location;
   });
			
}
