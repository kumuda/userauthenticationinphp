## Laravel PHP Framework

This is a student management project, in Php Laravel 5 framework.  

There are two types of user here.    
1. Admin  
2. Student.  

This application has Sign-Up, Sign-In and a HomePage.  

SignUp functionality will be only for student users. Whereas admin user will have only sign-In functionality.  

A student can sign-up but will not be enabled until admin sets the enable column to true in the user table.  

Once a user is logged in, depending on his/her roles, different Home pages will be shown.  
If Admin is logged in, then a list of all the students is shown in rows by pagination. On clicking any row, a modal   window will appear where admin can add, edit or delete student information.  

If a student is logged in. He can see only his information on Homepage, and he can add, edit/delete his/her details.      


Steps to run the application:    

Prerequisites: Mysql 5.* , PHP, Install Composer, Laravel 5.0, Apache server.

1. Clone the project by executing the below command:   
   git clone (type the URL given in right top corner of the page.)   

2. Create a database with name "student_management".     

3. Go to config/database.php file in the project folder, change the username and password in mysql array, in connections       array.   

4. To create the tables,run the migrations using below command:     
    php artisan migrate --force   

5. To add seed data, run the below command:    
   php artisan db:seed   

6. To run the application, type the below command:   
   php artisan serv